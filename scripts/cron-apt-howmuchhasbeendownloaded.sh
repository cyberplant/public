#!/bin/sh

if [ "$1"f == "f" ]; then
    echo "Usage: $0 {temp file of cron-apt}\n"
    echo "  Example: $0 /tmp/cron-apt.232/temp"
    exit 1
fi

BYTES="`grep '^Get' $1 | awk '{ print $6 }' | sed 's/\[//g;s/\]//g;s/kB/*1024/g;s/MB/*1024*1024/;s/B//g' | xargs echo | sed 's/ /+/g' | bc`"

echo "$BYTES bytes (`echo \"$BYTES / 1024\" | bc` kB)"

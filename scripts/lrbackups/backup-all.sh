#!/bin/sh

cd /mnt/backupdisk || exit 5

rsync --delete -avux / root/
rsync --delete -avux /bigfs/ bigfs/

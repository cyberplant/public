#!/bin/sh

FIFO="/var/run/lrbackup.fifo"
mkfifo $FIFO >/dev/null 2>&1
chmod 776 $FIFO

cd `dirname $0` 

while true; do
    echo "[`date`] Waiting trigger.. ";
    head -1 $FIFO
    echo "[`date`] Trigger found";
    ./backup-all.sh
    echo "[`date`] Backup finished .. ";
    sleep 1
done
